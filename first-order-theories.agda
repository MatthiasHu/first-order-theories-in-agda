
id : {A : Set₀} → A → A
id x = x

-- Dependently typed function composition in diagram order.
-- (‵ is \backprime)
_‵_ :
  ∀ {a b c} {A : Set a} {B : A → Set b} {C : {x : A} → B x → Set c} →
  (f : (x : A) → B x) →
  (∀ {x} (y : B x) → C y) →
  ((x : A) → C (f x))
(f ‵ g) x = g (f x)

infixr 20 _‵_

data void : Set₀ where

data unit : Set₀ where
  unique : unit

record pair (A B : Set₀) : Set₀ where
  constructor mk-pair
  field
    fst : A
    snd : B

data either (A B : Set₀) : Set₀ where
  left : A → either A B
  right : B → either A B

data Σ {A : Set₀} (B : A → Set₀) : Set₀ where
  _,_ : (a : A) → B a → Σ B

data list (A : Set₀) : Set₀ where
  [] : list A
  _∷_ : A → list A → list A

{-
map-list :
  ∀{A A'} →
  (A → A') →
  list A → list A'
map-list f [] = []
map-list f (x ∷ l) = f x ∷ map-list f l
-}

infixr 20 _∷_

data ℕ : Set₀ where
  zero : ℕ
  succ : ℕ → ℕ

data list-index {A} : list A → Set₀ where
  zero : ∀{a l} → list-index (a ∷ l)
  succ : ∀{a l} → list-index l → list-index (a ∷ l)

list-entry : {A : Set₀} {l : list A} → list-index l → A
list-entry (zero {a}) = a
list-entry (succ i) = list-entry i

index-number : {A : Set₀} {list : list A} → list-index list → ℕ
index-number zero = zero
index-number (succ index) = succ (index-number index)

-- A list of the same length as given list,
-- with values in types depending on the values of given list.
data list-family {A} (B : A → Set₀) : list A → Set₀ where
  [] : list-family B []
  _∷_ : ∀{l a} → B a → list-family B l → list-family B (a ∷ l)

list-family-entry :
  ∀{A B} {l : list A} →
  list-family B l →
  (i : list-index l) → B (list-entry i)
list-family-entry (b ∷ f) zero = b
list-family-entry (b ∷ f) (succ i) =
  list-family-entry f i

mk-list-family :
  ∀{A B} {l : list A} →
  ((i : list-index l) → B (list-entry i)) →
  list-family B l
mk-list-family {l = []} f = []
mk-list-family {l = _ ∷ _} f =
  (f zero) ∷ (mk-list-family (succ ‵ f))

map-list-family :
  ∀{A B C l} →
  ({a : A} → B a → C a) →
  list-family B l → list-family C l
map-list-family f [] = []
map-list-family f (b ∷ rest) =
  (f b) ∷ (map-list-family f rest)

{-
map-list-with-family :
  ∀{A A' B B' l} →
  (f : A → A') →
  ({a : A} → B a → B' (f a)) →
  list-family B l → list-family B' (map-list f l)
map-list-with-family f g [] = []
map-list-with-family f g (b ∷ bs) =
  g b ∷ map-list-with-family f g bs
-}

-- Is this good?
family-to-list :
  ∀{A B} {l : list A} →
  list-family B l →
  list (Σ B)
family-to-list [] = []
family-to-list (x ∷ l') = (_ , x) ∷ family-to-list l'

list-family' :
  ∀{A B l} →
  (C : {a : A} → B a → Set₀) →
  list-family B l →
  Set₀
list-family' C l' =
  list-family (λ {(_ , b) → C b}) (family-to-list l')

simplify-list-family :
  ∀{A B B'} {l : list A} →
  {l' : list-family B l} →
  list-family' (λ {a} _ → B' a) l' → list-family B' l
simplify-list-family {l' = []} [] = []
simplify-list-family {l' = _ ∷ _} (b' ∷ l'') =
  b' ∷ simplify-list-family l''

{-
list-product : ∀{A} → (l : list A) → (A → Set₀) → Set₀
list-product l B = (i : list-index l) → B (list-entry i)

-- It seems one always has to specify B explicitly here,
-- making this less useful.
extend-list-product :
  ∀{A l B} →
  list-product l B →
  (a : A) → B a →
  list-product (cons a l) B
extend-list-product f a b zero-index = b
extend-list-product f a b (succ-index i) = f i
-}

data _≡_ {ℓ} {A : Set ℓ} : A → A → Set₀ where
  refl : {a : A} → a ≡ a

cong : ∀{ℓ} {A B : Set ℓ} →
  (f : A → B) →
  {a₀ a₁ : A} →
  a₀ ≡ a₁ → f a₀ ≡ f a₁
cong f refl = refl

transport : {A B : Set₀} → A ≡ B → A → B
transport refl a = a

record first-order-signature : Set₁ where
  field
    sorts : Set₀
    functions : Set₀
    relations : Set₀
    function-domain : functions → list sorts
    function-codomain : functions → sorts
    relation-domain : relations → list sorts

open first-order-signature

-- It is probably bad to require sorts : Set₀ and similar.
-- I am writing down the simplest approach here,
-- since I don't know the most satisfactory one.
-- (I would linke to abstract over the notion of set,
-- so one can use sets of higher levels, but also setoids
-- and possibly other things.)

-- TODO: more wrapping, so function-domain and similar
-- don't need the signature, only the function symbol?

record contexts sig : Set₀ where
  constructor mk-context
  field run-context : list (sorts sig)

open contexts

extend-context : ∀{sig} → sorts sig → contexts sig → contexts sig
extend-context s c = mk-context (s ∷ (run-context c))

context-index : ∀{sig} → contexts sig → Set₀
context-index c = list-index (run-context c)

context-family : ∀{sig} → (sorts sig → Set₀) → contexts sig → Set₀
context-family B c = list-family B (run-context c)

-- Why am I feeling this way about singular vs plural?
-- "sorts" and "contexts", but "list-index", "first-order-signature" and "term"??

data term {sig} (c : contexts sig) : sorts sig → Set₀ where
  variable : (i : context-index c) → term c (list-entry i)
  function-application :
    (f : functions sig) →
    list-family (term c) (function-domain sig f) →
    term c (function-codomain sig f)

record term-induction-data {sig} (c : contexts sig) (A : ∀{s} → (t : term c s) → Set₀) : Set₀ where
  field
    variable-data :
      (i : context-index c) → A (variable i)
    function-application-data :
      (f : functions sig) →
      {ts : list-family (term c) (function-domain sig f)} →
      (rec-results : list-family' A ts) →
      A (function-application f ts)

open term-induction-data

term-induction :
  ∀{sig} {c : contexts sig} {A : ∀{s} → term c s → Set₀} →
  term-induction-data c A →
  ∀{s} →
  (t : term c s) → A t
map-term-induction :
  ∀{sig} {c : contexts sig} {A : ∀{s} → term c s → Set₀} →
  term-induction-data c A →
  ∀{ss} →
  (ts : list-family (term c) ss) → list-family' A ts
term-induction d (variable i) = variable-data d i
term-induction d (function-application f ts) =
  function-application-data d f {ts} (map-term-induction d ts)
map-term-induction d [] = []
map-term-induction d (t ∷ ts) =
  term-induction d t ∷ map-term-induction d ts

data atomic-formula {sig} (c : contexts sig) : Set₀ where
  relation-application :
    (r : relations sig) →
    list-family (term c) (relation-domain sig r) →
    atomic-formula c

-- We choose finitary first-order formulas here.
-- No big disjuctions or conjunctions,
-- but universal quantifiers are allowed
-- (unlike in geometric theories).
-- Oh, and without built-in equality.

data formula {sig} : contexts sig → Set₀ where
  -- atomic formulas
  atomic : ∀{c} → atomic-formula c → formula c
  -- conjunctions
  top : ∀{c} → formula c
  _and_ : ∀{c} → formula c → formula c → formula c
  -- disjuncions
  bot : ∀{c} → formula c
  _or_ : ∀{c} → formula c → formula c → formula c
  -- implication
  _implies_ : ∀{c} → formula c → formula c → formula c
  -- quantification
  for-all : ∀{c} → (s : sorts sig) → formula (extend-context s c) → formula c
  exists : ∀{c} → (s : sorts sig) → formula (extend-context s c) → formula c

record sequent (sig : first-order-signature) : Set₀ where
  constructor _⊢_
  field
    {context} : contexts sig
    assumption : formula context
    conclusion : formula context

record first-order-theory : Set₁ where
  field
    signature : first-order-signature
    axiom-names : Set₀
    axiom : axiom-names → sequent signature

open first-order-theory

-- exmaple theories

signature-of-propositions : Set₀ → first-order-signature
signature-of-propositions P = record
  { sorts = void
  ; functions = void
  ; relations = P
  ; function-domain = λ ()
  ; function-codomain = λ ()
  ; relation-domain = λ _ → []
  }

empty-theory : first-order-signature → first-order-theory
empty-theory sig = record
  { signature = sig
  ; axiom-names = void
  ; axiom = λ ()
  }

data monoid-functions : Set₀ where
  mempty : monoid-functions
  mappend : monoid-functions

monoid-function-domain : monoid-functions → list unit
monoid-function-domain mempty = []
monoid-function-domain mappend = unique ∷ unique ∷ []

monoid-signature : first-order-signature
monoid-signature = record
  { sorts = unit
  ; functions = monoid-functions
  ; relations = void  -- TODO: equality
  ; function-domain = monoid-function-domain
  ; function-codomain = λ _ → unique
  ; relation-domain = λ ()
  }

{-
-- general interpretations of theories

record preorder : Set₂ where
  field
    elements : Set₁
    leq : elements → elements → Set₀
    reflexivity : ∀{x} → leq x x
    -- (Do we need reflexivity for interpretations of theories??)
    transitivity : ∀{x y z} → leq x y → leq y z → leq x z 

inhabitedness-preorder : preorder
inhabitedness-preorder = record
  { elements = Set₀
  ; leq = λ X Y → (X → Y)
  ; reflexivity = λ x → x
  ; transitivity = λ f g x → g (f x)
  }

preorder-of-predicates : Set₀ → preorder
preorder-of-predicates X = record
  { elements = (X → Set₀)
  ; leq = λ p₀ p₁ → (∀{x} → p₀ x → p₁ x)
  ; reflexivity = λ y → y
  ; transitivity = λ f g y → g (f y)
  }

record signature-interpretation (sig : first-order-signature) : Set₂ where
  open preorder
  field
    interpret-context : contexts sig → preorder
    interpret-formula : ∀{c} → formula c → elements (interpret-context c)
    -- TODO: Gentzen-style inference rules are fulfilled

record interpretation (th : _) : Set₂ where
  field
    interpret-signature : signature-interpretation (signature th)
  open signature-interpretation
  open preorder
  i-f : ∀{c} → formula c → elements (interpret-context interpret-signature c)
  i-f ϕ = interpret-formula interpret-signature ϕ
  i-c : contexts (signature th) → preorder
  i-c c = interpret-context interpret-signature c
  open sequent
  field
    respect-axiom :
      (ax : axiom-names th) →
      let a = axiom th ax in
      leq (i-c (context a))
        (i-f (assumption a))
        (i-f (conclusion a))

inhabitedness-interpretation : ∀{P} → (P → Set₀) → signature-interpretation (signature-of-propositions P)
inhabitedness-interpretation p = record
  { interpret-context = λ _ → inhabitedness-preorder
  ; interpret-formula = {!!}
  }
-}

record rule (sig : first-order-signature) : Set₀ where
  constructor mk-rule
  field
    rule-prerequisites : list (sequent sig)
    rule-result : sequent sig

open rule

-- Gentzen style inference rules with the data they depend on.
-- (For the resulting prerequisites and results see below.)
data inference-rules (sig : first-order-signature) : Set₀ where
  -- structural
  tautology :
    {c : contexts sig} →
    formula c →
    inference-rules sig
  cut :
    {c : contexts sig} →
    formula c → formula c → formula c →
    inference-rules sig
  -- variables
  substitution :
    {c c' : contexts sig} →
    formula c → formula c →
    context-family (term c') c →
    inference-rules sig
  -- conjunction
  top-intro :
    {c : contexts sig} →
    formula c →
    inference-rules sig
  and-intro :
    {c : contexts sig} →
    formula c → formula c → formula c →
    inference-rules sig
  and-elim₁ :
    {c : contexts sig} →
    formula c → formula c →
    inference-rules sig
  and-elim₂ :
    {c : contexts sig} →
    formula c → formula c →
    inference-rules sig
  -- disjunction
  bottom-elim :
    {c : contexts sig} →
    formula c →
    inference-rules sig
  or-intro₁ :
    {c : contexts sig} →
    formula c → formula c →
    inference-rules sig
  or-intro₂ :
    {c : contexts sig} →
    formula c → formula c →
    inference-rules sig
  or-elim :
    {c : contexts sig} →
    formula c → formula c → formula c →
    inference-rules sig
  -- implication
  implies-intro :
    {c : contexts sig} →
    formula c → formula c → formula c →
    inference-rules sig
  implies-elim :
    {c : contexts sig} →
    formula c → formula c → formula c →
    inference-rules sig
  -- universal quantification
  for-all-intro :
    {c : contexts sig} {s : sorts sig} →
    formula c → formula (extend-context s c) →
    inference-rules sig
  for-all-elim :
    {c : contexts sig} {s : sorts sig} →
    formula c → formula (extend-context s c) →
    inference-rules sig
  -- existential quantification
  exists-intro :
    {c : contexts sig} {s : sorts sig} →
    formula (extend-context s c) → formula c →
    inference-rules sig
  exists-elim :
    {c : contexts sig} {s : sorts sig} →
    formula (extend-context s c) → formula c →
    inference-rules sig

substitute-term-variables :
  ∀{sig} {c c' : contexts sig} {s} →
  context-family (term c') c →
  term c s → term c' s
substitute-term-variables ts = term-induction record
  { variable-data = λ i → list-family-entry ts i
  ; function-application-data = λ f rec-results →
      function-application f (simplify-list-family rec-results)
  }

promotion-substitution :
  ∀{sig} {c : contexts sig} {s} →
  context-family (term (extend-context s c)) c
promotion-substitution =
  mk-list-family (succ ‵ variable)

promote-term :
  ∀{sig} {c : contexts sig} {s' s} →
  term c s → term (extend-context s' c) s
promote-term {c = c} {s'} = term-induction  (record
  { variable-data = λ i → variable (succ i)
  ; function-application-data = λ f rec-results →
      function-application f (simplify-list-family rec-results)
  })
-- promote-term = substitute-term-variables promotion-substitution

augment-term-family :
  ∀{sig} {c c' : contexts sig} {s} →
  context-family (term c') c →
  context-family (term (extend-context s c')) (extend-context s c)
augment-term-family {sig} {c} {c'} ts =
  variable zero ∷ map-list-family promote-term ts

substitute-free-variables :
  ∀{sig} {c c' : contexts sig} →
  context-family (term c') c →
  formula c → formula c'
substitute-free-variables ts (atomic (relation-application r args)) =
  atomic (relation-application r
    (map-list-family (substitute-term-variables ts) args))
substitute-free-variables ts top = top
substitute-free-variables ts (f₁ and f₂) =
  (substitute-free-variables ts f₁) and
  (substitute-free-variables ts f₂)
substitute-free-variables ts bot = bot
substitute-free-variables ts (f₁ or f₂) =
  (substitute-free-variables ts f₁) or
  (substitute-free-variables ts f₂)
substitute-free-variables ts (f₁ implies f₂) =
  (substitute-free-variables ts f₁) implies
  (substitute-free-variables ts f₂)
substitute-free-variables ts (for-all s f) =
  for-all s (substitute-free-variables (augment-term-family ts) f)
substitute-free-variables ts (exists s f) =
  exists s (substitute-free-variables (augment-term-family ts) f)

promote-formula :
  ∀{sig} {c : contexts sig} {s} →
  formula c → formula (extend-context s c)
promote-formula = substitute-free-variables promotion-substitution

rule-content : ∀{sig} → inference-rules sig → rule sig
rule-content (tautology a) = mk-rule [] (a ⊢ a)
rule-content (cut a b c) = mk-rule
  ((a ⊢ b) ∷ (b ⊢ c) ∷ [])
  (a ⊢ c)
rule-content (substitution a b ts) = mk-rule
  ((a ⊢ b) ∷ [])
  (substitute-free-variables ts a ⊢ substitute-free-variables ts b)
rule-content (top-intro a) = mk-rule [] (a ⊢ top)
rule-content (and-intro a b c) = mk-rule
  ((a ⊢ b) ∷ (a ⊢ c) ∷ [])
  (a ⊢ (b and c))
rule-content (and-elim₁ a b) = mk-rule [] ((a and b) ⊢ a)
rule-content (and-elim₂ a b) = mk-rule [] ((a and b) ⊢ b)
rule-content (bottom-elim a) = mk-rule [] (bot ⊢ a)
rule-content (or-intro₁ a b) = mk-rule [] (a ⊢ (a or b))
rule-content (or-intro₂ a b) = mk-rule [] (b ⊢ (a or b))
rule-content (or-elim a b c) = mk-rule
  ((a ⊢ c) ∷ (b ⊢ c) ∷ [])
  ((a or b) ⊢ c)
rule-content (implies-intro a b c) = mk-rule
  (((a and b) ⊢ c) ∷ [])
  (a ⊢ (b implies c))
rule-content (implies-elim a b c) = mk-rule
  ((a ⊢ (b implies c)) ∷ [])
  ((a and b) ⊢ c)
rule-content (for-all-intro a b) = mk-rule
  ((promote-formula a ⊢ b) ∷ [])
  (a ⊢ (for-all _ b))
rule-content (for-all-elim a b) = mk-rule
  ((a ⊢ for-all _ b) ∷ [])
  (promote-formula a ⊢ b)
rule-content (exists-intro a b) = mk-rule
  ((exists _ a ⊢ b) ∷ [])
  (a ⊢ promote-formula b)
rule-content (exists-elim a b) = mk-rule
  ((a ⊢ promote-formula b) ∷ [])
  (exists _ a ⊢ b)

prerequisites : ∀{sig} → inference-rules sig → list (sequent sig)
prerequisites = λ r → rule-prerequisites (rule-content r)

result : ∀{sig} → inference-rules sig → sequent sig
result = λ r → rule-result (rule-content r)

record interpretation (th : first-order-theory) : Set₁ where
  field
    validity : sequent (signature th) → Set₀
    inference-rule-valid :
      (r : inference-rules (signature th)) →
      list-family validity (prerequisites r) →
      validity (result r)
    axiom-valid :
      (ax : axiom-names th) →
      validity (axiom th ax)

open interpretation

data proof (th : _) : sequent (signature th) → Set₀ where
  by-axiom :
    (ax : axiom-names th) →
    proof th (axiom th ax)
  by-rule :
    (r : inference-rules (signature th)) →
    list-family (proof th) (prerequisites r) →
    proof th (result r)

-- Just trying this to nicely proof soundness.
record proof-induction-data (th : _) (A : ∀{seq} → proof th seq → Set₀) : Set₀ where
  field
    by-axiom-data :
      (ax : axiom-names th) →
      A (by-axiom ax)
    by-rule-data :
      (r : inference-rules (signature th)) →
      {ps : list-family (proof th) (prerequisites r)} →
      (rec-results : list-family' A ps) →
      A (by-rule r ps)

open proof-induction-data

proof-induction :
  ∀{th : first-order-theory} {A : ∀{seq} → proof th seq → Set₀} →
  proof-induction-data th A →
  ∀{seq} →
  (p : proof th seq) → A p
map-proof-induction :
  ∀{th : first-order-theory} {A : ∀{seq} → proof th seq → Set₀} →
  proof-induction-data th A →
  ∀{seqs} →
  (ps : list-family (proof th) seqs) → list-family' A ps
proof-induction d (by-axiom ax) =
  by-axiom-data d ax
proof-induction d (by-rule r ps) =
  by-rule-data d r {ps} (map-proof-induction d ps)
map-proof-induction d [] = []
map-proof-induction d (p ∷ ps) =
  proof-induction d p ∷ map-proof-induction d ps

soundness :
  ∀{th seq} →
  (I : interpretation th) →
  proof th seq → validity I seq
soundness I = proof-induction record
  { by-axiom-data = axiom-valid I
  ; by-rule-data = λ r rec-results →
      inference-rule-valid I r (simplify-list-family rec-results)
  }

record structure sig : Set₁ where
  field
    sort : sorts sig → Set₀
    function :
      (f : functions sig) →
      list-family sort (function-domain sig f) →
      sort (function-codomain sig f)
    relation :
      (r : relations sig) →
      list-family sort (relation-domain sig r) →
      Set₀

open structure

term-in-structure :
  ∀{sig} {c : contexts sig} {s} →
  (str : structure sig) →
  context-family (sort str) c →
  term c s →
  sort str s
term-in-structure str xs = term-induction record
  { variable-data = λ i → list-family-entry xs i
  ; function-application-data = λ f ys →
      function str f (simplify-list-family ys)
  }

{-
promoted-variable :
  ∀{sig} {c : contexts sig} {i : context-index c} {s'} →
  promote-term {sig = sig} {s' = s'} (variable i) ≡ variable (succ i)
promoted-variable = refl
-}

promoted-term-in-structure :
  ∀{sig} {c : contexts sig} {s} {s'} →
  (str : structure sig) →
  (xs : context-family (sort str) c) →
  (x : sort str s') →
  (t : term c s) →
  term-in-structure str (x ∷ xs) (promote-term t) ≡
  term-in-structure str xs t
promoted-term-in-structure str xs x = term-induction
  {A = λ t → term-in-structure str (x ∷ xs) (promote-term t) ≡ term-in-structure str xs t}
  (record
  { variable-data = λ i → refl -- cong (term-in-structure str (x ∷ xs)) promoted-variable
  ; function-application-data = λ f {ts} rec-results → cong (function str f) {!
      id {map-list-family (term-in-structure str (x ∷ xs)) (map-list-family promote-term ts) ≡ map-list-family (term-in-structure str xs) ts} ?!} })

formula-in-structure :
  ∀{sig} {c : contexts sig} →
  (str : structure sig) →
  formula c →
  context-family (sort str) c → Set₀
formula-in-structure str (atomic (relation-application r args)) xs =
  relation str r
    (map-list-family (λ t → term-in-structure str xs t) args)
formula-in-structure str top xs = unit
formula-in-structure str (f₁ and f₂) xs =
  pair (formula-in-structure str f₁ xs)
       (formula-in-structure str f₂ xs)
formula-in-structure str bot xs = void
formula-in-structure str (f₁ or f₂) xs =
  either (formula-in-structure str f₁ xs)
         (formula-in-structure str f₂ xs)
formula-in-structure str (f₁ implies f₂) xs =
  formula-in-structure str f₁ xs →
  formula-in-structure str f₂ xs
formula-in-structure str (for-all s f) xs =
  (x : _) → formula-in-structure str f (x ∷ xs)
formula-in-structure str (exists s f) xs =
  Σ λ x → formula-in-structure str f (x ∷ xs)

valid-in-structure : ∀{sig} → structure sig → sequent sig → Set₀
valid-in-structure str (f₁ ⊢ f₂) =
  ∀{xs} →
  formula-in-structure str f₁ xs →
  formula-in-structure str f₂ xs

record model th : Set₁ where
  sig = signature th
  field
    model-structure : structure sig
    axiom-fulfilled :
      (ax : axiom-names th) →
      valid-in-structure model-structure (axiom th ax)

open model

-- Would like to make this obsolete by promoted-formula-in-structure...
promote-witness :
  ∀{sig} {c : contexts sig} →
  {str : structure sig} →
  (f : formula c) →
  {xs : context-family (sort str) c} →
  {s : sorts sig} →
  {x : sort str s} →
  formula-in-structure str f xs →
  formula-in-structure str (promote-formula f) (x ∷ xs)
unpromote-witness :
  ∀{sig} {c : contexts sig} →
  {str : structure sig} →
  (f : formula c) →
  {xs : context-family (sort str) c} →
  {s : sorts sig} →
  {x : sort str s} →
  formula-in-structure str (promote-formula f) (x ∷ xs) →
  formula-in-structure str f xs

promote-witness = λ
  { (atomic (relation-application r ts)) w → {!!}
  ; top → id
  ; (f₁ and f₂) (mk-pair w₁ w₂) →
      mk-pair (promote-witness f₁ w₁) (promote-witness f₂ w₂)
  ; bot → id
  ; (f₁ or f₂) (left w) → left (promote-witness f₁ w)
  ; (f₁ or f₂) (right w) → right (promote-witness f₂ w)
  ; (f₁ implies f₂) g w → promote-witness f₂ (g (unpromote-witness f₁ w))
  -- Can't we use composition here? Is it too polymorphic?
  ; (for-all s f) w → {!!}
  ; (exists s f) w → {!!}
  }
unpromote-witness = {!!}

rule-valid-in-structure :
  ∀{sig} →
  (str : structure sig) →
  (r : inference-rules sig) →
  list-family (valid-in-structure str) (prerequisites r) →
  valid-in-structure str (result r)
rule-valid-in-structure str = λ
  { (tautology f) [] → id
  ; (cut f₁ f₂ f₃) (g₁ ∷ g₂ ∷ []) → g₁ ‵ g₂
  ; (substitution f₁ f₂ ts) (g ∷ []) → {!!}
  ; (top-intro f) [] _ → unique
  ; (and-intro f₁ f₂ f₃) (g₁ ∷ g₂ ∷ []) w → mk-pair (g₁ w) (g₂ w)
  ; (and-elim₁ f₁ f₂) [] → pair.fst
  ; (and-elim₂ f₁ f₂) [] → pair.snd
  ; (bottom-elim f) [] → λ ()
  ; (or-intro₁ f₁ f₂) [] → left
  ; (or-intro₂ f₁ f₂) [] → right
  ; (or-elim f₁ f₂ f₃) (g₁ ∷ g₂ ∷ []) →
      λ { (left w)  → g₁ w
        ; (right w) → g₂ w }
  ; (implies-intro f₁ f₂ f₃) (g ∷ []) w₁ w₂ → g (mk-pair w₁ w₂)
  ; (implies-elim f₁ f₂ f₃) (g ∷ []) (mk-pair w₁ w₂) → g w₁ w₂
  ; (for-all-intro f₁ f₂) (g ∷ []) →
      λ w _ → g (promote-witness f₁ w)
  ; (for-all-elim f₁ f₂) (g ∷ []) {xs = x ∷ _} →
      λ w → g (unpromote-witness f₁ w) x
  ; (exists-intro f f₂) (g ∷ []) {xs = x ∷ _} →
      λ w → promote-witness f₂ (g (x , w))
  ; (exists-elim f₁ f₂) (g ∷ []) →
      λ { (x , w) → unpromote-witness f₂ (g w) }
  }

model-interpretation : ∀{th} → model th → interpretation th
model-interpretation m =
  let str = model-structure m in
  record
    { validity = valid-in-structure str
    ; inference-rule-valid = rule-valid-in-structure str
    ; axiom-valid = axiom-fulfilled m
    }
